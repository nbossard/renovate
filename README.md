# GitLab CI template for Renovate

Automate your dependency updates with [Renovate](https://www.mend.io/renovate/).

## Usage

In order to include this template in your project, add the following to your `gitlab-ci.yml`:

```yaml
include:
  - project: 'to-be-continuous/renovate'
    ref: '1.0.0'
    file: '/templates/gitlab-ci-renovate.yml'
```

## Configuration

The Renovate template uses some global configuration used throughout all jobs.

| Name                   | Description                                                                     | Default value     |
|------------------------|---------------------------------------------------------------------------------|-------------------|
| `RENOVATE_IMAGE`       | The Docker image used to run Renovate                                           | `registry.hub.docker.com/renovate/renovate:latest` |
| :lock: `RENOVATE_TOKEN`| A GitLab access token to allow Renovate crawl your projects. [See doc](https://docs.renovatebot.com/modules/platform/gitlab/#authentication) | _none_ |
| :lock: `GITHUB_COM_TOKEN`| A GitHub access token to allow Renovate fetch changelogs. [See doc](https://docs.renovatebot.com/getting-started/running/#githubcom-token-for-changelogs) | _none_ |

This template will help you using [Renovate](https://www.mend.io/renovate/) from a GitLab project to
automate your dependency updates within your groups or projects.
On the contrary to other to-be-continuous templates, this one should be used in a separate project that
will be in charge of crawling all your other projects.

Upon including the template, carefuly follow [Renovate's documentation](https://docs.renovatebot.com/) to
configure the bot accordingly. Pay attention to the following:

* Remember to set the [platform](https://docs.renovatebot.com/self-hosted-configuration/#platform) parameter
  to `gitlab` in your configuration.
* [GitLab platform integration](https://docs.renovatebot.com/modules/platform/gitlab/) requires that you
  declare a `RENOVATE_TOKEN` variable with an access token.
* You'll also probaly need to declare a `GITHUB_COM_TOKEN` variable, holding a GitHub access token 
  (for [fetching changelogs](https://docs.renovatebot.com/getting-started/running/#githubcom-token-for-changelogs))
